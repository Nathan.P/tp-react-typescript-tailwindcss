import { ReactChildren, ReactChild } from "react";

interface AuxProps {
  children: ReactChild | ReactChild[] | ReactChildren | ReactChildren[];
  title: string;
}

const Grid = ({ children, title }: AuxProps) => {
  return (
    <div className="section section-xl">
      <h1 className="section__title">{title}</h1>
      <div className="grid--responsive">{children}</div>
    </div>
  );
};

export default Grid;
