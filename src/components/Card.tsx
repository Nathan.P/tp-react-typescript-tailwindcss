import Result from "../interfaces/Result";

interface AuxProps {
  data: Result;
}

const Card = ({ data }: AuxProps) => {
  const { id, value, img }: Result = data;
  return (
    <div className="card">
      <figure className="card__img">
        <img src={img} alt="chunk noris img" />
      </figure>
      <div className="card__body">
        <h2 className="card__title">ID: {id}</h2>
        <p className="card__quote">{value}</p>
      </div>
    </div>
  );
};

export default Card;
