interface Result {
  categories: Array<string>;
  created_at: string;
  icon_url: string;
  id: string;
  updated_at: string;
  url: string;
  img: string;
  value: string;
}

export default Result;
