import { useCallback, useEffect, useState } from "react";
import Card from "./components/Card";
import Grid from "./components/Grid";
import Result from "./interfaces/Result";
import Img from "./assets/img.json";

const App = () => {
  const gridTitle: string = "Chunk Noris Facts";
  const [data, setData] = useState<Array<Result> | undefined>();

  useEffect(() => {
    async function fetchData() {
      const results: Array<Result> = [];
      for (let i = 0; i < 10; i++) {
        const response = await fetch("https://api.chucknorris.io/jokes/random");
        const json: Result = await response.json();
        json.img = Img[Math.floor(Math.random() * Img.length)];
        results.push(json);
      }
      setData(results);
    }
    fetchData();
  }, []);

  const cards = useCallback(() => {
    if (data)
      return data.map((result: Result, index: number) => (
        <Card key={index} data={result} />
      ));
    else return <div className="nothing"></div>;
  }, [data]);

  return (
    <div className="main">
      <Grid title={gridTitle}>{cards()}</Grid>
    </div>
  );
};

export default App;
